import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import {CommandeService} from './commande.service';
import {CreateCommandeDto} from './dto/commande.dto';

@Controller('commande')
export class CommandeController {
    constructor(private commandeService: CommandeService) {}

    // Add commande: /commande/create
    @Post('/create')
    async createCommande(@Res() res, @Body() createCommandeDto: CreateCommandeDto) {
        const commande = await this.commandeService.createCommande(createCommandeDto);
        return res.status(HttpStatus.OK).json({
            message: 'Commande Successfully Created',
            commande
        });
    }

    // Get Commandes /commande
    // @Get('/list')
    @Get('/')
    async getCommandes(@Res() res) {
        const commandes = await this.commandeService.getCommandes();
        return res.status(HttpStatus.OK).json(commandes);
    }

    // GET single product: /commande/5c9d46100e2e5c44c444b2d1
    @Get('/:commandeID')
    async getCommande(@Res() res, @Param('commandeID') commandeID) {
        const commande = await this.commandeService.getCommande(commandeID);
        if (!commande) throw new NotFoundException('Commande does not exist!');
        return res.status(HttpStatus.OK).json(commande);
    }

    // Delete Commande: /delete?commandeID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteCommande(@Res() res, @Query('commandeID') commandeID) {
        const commandeDeleted = await this.commandeService.deleteCommande(commandeID);
        if (!commandeDeleted) throw new NotFoundException('Commande does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Commande Deleted Successfully',
            commandeDeleted
        });
    }

    // Update Commande: /update?commandeID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createCommandeDTO: CreateCommandeDto, @Query('commandeID') commandeID) {
        const updatedCommande = await this.commandeService.updateCommande(commandeID, createCommandeDTO);
        if (!updatedCommande) throw new NotFoundException('Commande does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Commande Updated Successfully',
            updatedCommande
        });
    }
}
