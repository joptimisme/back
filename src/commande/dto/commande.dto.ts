export class CreateCommandeDto {
    readonly id_user: String;
    readonly id_proc: String;
    readonly id_alim: String;
    readonly id_ram: String;
    readonly id_vent: String;
    readonly id_cg: String;
    readonly id_cm: String;
    readonly id_case: String;
    readonly id_storage: String;
    readonly prix: Number;
    readonly createdAt: Date;
  }
