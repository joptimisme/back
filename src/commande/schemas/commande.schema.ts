import { Schema } from 'mongoose';

export const CommandeSchema = new Schema({
    id_user: ({type: String , required: true }),
    id_proc: String,
    id_alim: String,
    id_ram: String,
    id_vent: String,
    id_cg: String,
    id_cm: String,
    id_case: String,
    id_storage: String,
    prix: ({type: Number , required: true }),
    createdAt: { type: Date, default: Date.now },
}, {
    versionKey: false,
});
