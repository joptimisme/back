import { Module } from '@nestjs/common';
import { CommandeService } from './commande.service';
import { CommandeController } from './commande.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CommandeSchema } from './schemas/commande.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Commande', schema: CommandeSchema }])],
    providers: [CommandeService],
    controllers: [CommandeController],
})
export class CommandeModule {}
