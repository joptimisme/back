import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Commande } from './interfaces/commande.interface';
import { CreateCommandeDto } from './dto/commande.dto';

@Injectable()
export class CommandeService {
    constructor(@InjectModel('Commande') private readonly commandeModel: Model<Commande>) {}

    // Get all products
    async getCommandes(): Promise<Commande[]> {
        const commandes = await this.commandeModel.find();
        return commandes;
    }

    // Get a single Product
    async getCommande(commandeID: string): Promise<Commande> {
        const commande = await this.commandeModel.findById(commandeID);
        return commande;
    }

    // Post a single product
    async createCommande(createCommandeDTO: CreateCommandeDto): Promise<Commande> {
        const newCommande = new this.commandeModel(createCommandeDTO);
        return newCommande.save();
    }

    // Delete Commande
    async deleteCommande(commandeID: string): Promise<any> {
        const deleteCommande = await this.commandeModel.findOneAndDelete(commandeID);
        return deleteCommande;
    }

    // Put a single commande
    async updateCommande(commandeID: string, createCommandeDTO: CreateCommandeDto): Promise<Commande> {
        const updatedCommande = await this.commandeModel
            .findByIdAndUpdate(commandeID, createCommandeDTO, {new: true});
        return updatedCommande;
    }
}
