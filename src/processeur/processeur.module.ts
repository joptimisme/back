import { Module } from '@nestjs/common';
import { ProcesseurService } from './processeur.service';
import { ProcesseurController } from './processeur.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ProcesseurSchema } from './schemas/processeur.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Processeur', schema: ProcesseurSchema }])],
    providers: [ProcesseurService],
    controllers: [ProcesseurController],
})
export class ProcesseurModule {}
