import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Processeur} from './interfaces/processeur.interface';
import {CreateProcesseurDto} from './dto/processeur.dto';

@Injectable()
export class ProcesseurService {
    constructor(@InjectModel('Processeur') private readonly processeurModel: Model<Processeur>) {}

    // Get all products
    async getProcesseurs(): Promise<Processeur[]> {
        const processeurs = await this.processeurModel.find();
        return processeurs;
    }

    // Get a single Product
    async getProcesseur(processeurID: string): Promise<Processeur> {
        const processeur = await this.processeurModel.findById(processeurID);
        return processeur;
    }

    // Post a single product
    async createProcesseur(createProcesseurDTO: CreateProcesseurDto): Promise<Processeur> {
        const newProcesseur = new this.processeurModel(createProcesseurDTO);
        return newProcesseur.save();
    }

    // Delete Processeur
    async deleteProcesseur(processeurID: string): Promise<any> {
        const deleteProcesseur = await this.processeurModel.findOneAndDelete(processeurID);
        return deleteProcesseur;
    }

    // Put a single processeur
    async updateProcesseur(processeurID: string, createProcesseurDTO: CreateProcesseurDto): Promise<Processeur> {
        const updatedProcesseur = await this.processeurModel
            .findByIdAndUpdate(processeurID, createProcesseurDTO, {new: true});
        return updatedProcesseur;
    }
}
