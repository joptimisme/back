import { Document } from 'mongoose';

export interface Processeur extends Document {
    readonly marque: String;
    readonly modele: String;
    readonly socket: String;
    readonly tdp: Number;
    readonly prix: Number;
}