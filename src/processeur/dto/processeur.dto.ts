export class CreateProcesseurDto {
    readonly marque: String;
    readonly modele: String;
    readonly socket: String;
    readonly tdp: Number;
    readonly prix: Number;
  }
