import { Test, TestingModule } from '@nestjs/testing';
import { ProcesseurController } from './processeur.controller';

describe('Processeur Controller', () => {
  let controller: ProcesseurController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProcesseurController],
    }).compile();

    controller = module.get<ProcesseurController>(ProcesseurController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
