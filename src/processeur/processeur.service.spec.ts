import { Test, TestingModule } from '@nestjs/testing';
import { ProcesseurService } from './processeur.service';

describe('ProcesseurService', () => {
  let service: ProcesseurService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProcesseurService],
    }).compile();

    service = module.get<ProcesseurService>(ProcesseurService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
