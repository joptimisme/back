import { Schema } from 'mongoose';

export const ProcesseurSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    socket: ({type: String , required: true }),
    tdp: ({type: Number , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});