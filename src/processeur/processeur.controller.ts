import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import {ProcesseurService} from './processeur.service';
import {CreateProcesseurDto} from './dto/processeur.dto';

@Controller('processeur')
export class ProcesseurController {
    constructor(private processeurService: ProcesseurService) {}

    // Add processeur: /processeur/create
    @Post('/create')
    async createProcesseur(@Res() res, @Body() createProcesseurDto: CreateProcesseurDto) {
        const processeur = await this.processeurService.createProcesseur(createProcesseurDto);
        return res.status(HttpStatus.OK).json({
            message: 'Processeur Successfully Created',
            processeur
        });
    }

    // Get Processeurs /processeur
    // @Get('/list')
    @Get('/')
    async getProcesseurs(@Res() res) {
        const processeurs = await this.processeurService.getProcesseurs();
        return res.status(HttpStatus.OK).json(processeurs);
    }

    // GET single product: /processeur/5c9d46100e2e5c44c444b2d1
    @Get('/:processeurID')
    async getProcesseur(@Res() res, @Param('processeurID') processeurID) {
        const processeur = await this.processeurService.getProcesseur(processeurID);
        if (!processeur) throw new NotFoundException('Processeur does not exist!');
        return res.status(HttpStatus.OK).json(processeur);
    }

    // Delete Processeur: /delete?processeurID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteProcesseur(@Res() res, @Query('processeurID') processeurID) {
        const processeurDeleted = await this.processeurService.deleteProcesseur(processeurID);
        if (!processeurDeleted) throw new NotFoundException('Processeur does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Processeur Deleted Successfully',
            processeurDeleted
        });
    }

    // Update Processeur: /update?processeurID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createProcesseurDTO: CreateProcesseurDto, @Query('processeurID') processeurID) {
        const updatedProcesseur = await this.processeurService.updateProcesseur(processeurID, createProcesseurDTO);
        if (!updatedProcesseur) throw new NotFoundException('Processeur does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Processeur Updated Successfully',
            updatedProcesseur
        });
    }
}
