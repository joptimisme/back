import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { RamService } from './ram.service';
import { CreateRamDto } from './dto/ram.dto';

@Controller('ram')
export class RamController {
    constructor(private ramService: RamService) {}

    // Add ram: /ram/create
    @Post('/create')
    async createRam(@Res() res, @Body() createRamDto: CreateRamDto) {
        const ram = await this.ramService.createRam(createRamDto);
        return res.status(HttpStatus.OK).json({
            message: 'Ram Successfully Created',
            ram
        });
    }

    // Get Rams /ram
    // @Get('/list')
    @Get('/')
    async getRams(@Res() res) {
        const rams = await this.ramService.getRams();
        return res.status(HttpStatus.OK).json(rams);
    }

    // GET single product: /ram/5c9d46100e2e5c44c444b2d1
    @Get('/:ramID')
    async getRam(@Res() res, @Param('ramID') ramID) {
        const ram = await this.ramService.getRam(ramID);
        if (!ram) throw new NotFoundException('Ram does not exist!');
        return res.status(HttpStatus.OK).json(ram);
    }

    // Delete Ram: /delete?ramID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteRam(@Res() res, @Query('ramID') ramID) {
        const ramDeleted = await this.ramService.deleteRam(ramID);
        if (!ramDeleted) throw new NotFoundException('Ram does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Ram Deleted Successfully',
            ramDeleted
        });
    }

    // Update Ram: /update?ramID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createRamDTO: CreateRamDto, @Query('ramID') ramID) {
        const updatedRam = await this.ramService.updateRam(ramID, createRamDTO);
        if (!updatedRam) throw new NotFoundException('Ram does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Ram Updated Successfully',
            updatedRam
        });
    }
}
