import { Module } from '@nestjs/common';
import { RamService } from './ram.service';
import { RamController } from './ram.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { RamSchema } from './schemas/ram.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Ram', schema: RamSchema }])],
    providers: [RamService],
    controllers: [RamController],
})
export class RamModule {}
