import { Schema } from 'mongoose';

export const RamSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    frequence: ({type: Number , required: true }),
    capacite: ({type: Number , required: true }),
    format: ({type: String , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});