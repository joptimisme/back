import { Document } from 'mongoose';

export interface Ram extends Document {
    readonly marque: String;
    readonly modele: String;
    readonly frequence: Number;
    readonly capacite: Number;
    readonly format: String;
    readonly prix: Number;
}