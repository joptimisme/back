import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Ram} from './interfaces/ram.interface';
import {CreateRamDto} from './dto/ram.dto';

@Injectable()
export class RamService {
    constructor(@InjectModel('Ram') private readonly ramModel: Model<Ram>) {}

    // Get all products
    async getRams(): Promise<Ram[]> {
        const rams = await this.ramModel.find();
        return rams;
    }

    // Get a single Product
    async getRam(ramID: string): Promise<Ram> {
        const ram = await this.ramModel.findById(ramID);
        return ram;
    }

    // Post a single product
    async createRam(createRamDTO: CreateRamDto): Promise<Ram> {
        const newRam = new this.ramModel(createRamDTO);
        return newRam.save();
    }

    // Delete Ram
    async deleteRam(ramID: string): Promise<any> {
        const deleteRam = await this.ramModel.findOneAndDelete(ramID);
        return deleteRam;
    }

    // Put a single ram
    async updateRam(ramID: string, createRamDTO: CreateRamDto): Promise<Ram> {
        const updatedRam = await this.ramModel
            .findByIdAndUpdate(ramID, createRamDTO, {new: true});
        return updatedRam;
    }
}
