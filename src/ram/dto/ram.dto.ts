export class CreateRamDto {
    readonly marque: String;
    readonly modele: String;
    readonly frequence: Number;
    readonly capacite: Number;
    readonly format: String;
    readonly prix: Number;
  }
