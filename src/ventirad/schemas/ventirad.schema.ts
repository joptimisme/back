import { Schema } from 'mongoose';

export const VentiradSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    tdp: ({type: Number , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});