import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Ventirad } from './interfaces/ventirad.interface';
import { CreateVentiradDto } from './dto/ventirad.dto';

@Injectable()
export class VentiradService {
    constructor(@InjectModel('Ventirad') private readonly ventiradModel: Model<Ventirad>) {}

    // Get all products
    async getVentirads(): Promise<Ventirad[]> {
        const ventirads = await this.ventiradModel.find();
        return ventirads;
    }

    // Get a single Product
    async getVentirad(ventiradID: string): Promise<Ventirad> {
        const ventirad = await this.ventiradModel.findById(ventiradID);
        return ventirad;
    }

    // Post a single product
    async createVentirad(createVentiradDTO: CreateVentiradDto): Promise<Ventirad> {
        const newVentirad = new this.ventiradModel(createVentiradDTO);
        return newVentirad.save();
    }

    // Delete Ventirad
    async deleteVentirad(ventiradID: string): Promise<any> {
        const deleteVentirad = await this.ventiradModel.findOneAndDelete(ventiradID);
        return deleteVentirad;
    }

    // Put a single ventirad
    async updateVentirad(ventiradID: string, createVentiradDTO: CreateVentiradDto): Promise<Ventirad> {
        const updatedVentirad = await this.ventiradModel
            .findByIdAndUpdate(ventiradID, createVentiradDTO, {new: true});
        return updatedVentirad;
    }
}
