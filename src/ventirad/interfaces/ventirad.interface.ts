import { Document } from 'mongoose';

export interface Ventirad extends Document {
    readonly marque: String;
    readonly modele: String;
    readonly tdp: Number;
    readonly prix: Number;
}