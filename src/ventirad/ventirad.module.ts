import { Module } from '@nestjs/common';
import { VentiradService } from './ventirad.service';
import { VentiradController } from './ventirad.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { VentiradSchema } from './schemas/ventirad.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Ventirad', schema: VentiradSchema }])],
    providers: [VentiradService],
    controllers: [VentiradController],
})
export class VentiradModule {}
