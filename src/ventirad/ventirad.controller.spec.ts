import { Test, TestingModule } from '@nestjs/testing';
import { VentiradController } from './ventirad.controller';

describe('Ventirad Controller', () => {
  let controller: VentiradController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VentiradController],
    }).compile();

    controller = module.get<VentiradController>(VentiradController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
