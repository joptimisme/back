import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { VentiradService } from './ventirad.service';
import { CreateVentiradDto } from './dto/ventirad.dto';

@Controller('ventirad')
export class VentiradController {
    constructor(private ventiradService: VentiradService) {}

    // Add ventirad: /ventirad/create
    @Post('/create')
    async createVentirad(@Res() res, @Body() createVentiradDto: CreateVentiradDto) {
        const ventirad = await this.ventiradService.createVentirad(createVentiradDto);
        return res.status(HttpStatus.OK).json({
            message: 'Ventirad Successfully Created',
            ventirad
        });
    }

    // Get Ventirads /ventirad
    // @Get('/list')
    @Get('/')
    async getVentirads(@Res() res) {
        const ventirads = await this.ventiradService.getVentirads();
        return res.status(HttpStatus.OK).json(ventirads);
    }

    // GET single product: /ventirad/5c9d46100e2e5c44c444b2d1
    @Get('/:ventiradID')
    async getVentirad(@Res() res, @Param('ventiradID') ventiradID) {
        const ventirad = await this.ventiradService.getVentirad(ventiradID);
        if (!ventirad) throw new NotFoundException('Ventirad does not exist!');
        return res.status(HttpStatus.OK).json(ventirad);
    }

    // Delete Ventirad: /delete?ventiradID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteVentirad(@Res() res, @Query('ventiradID') ventiradID) {
        const ventiradDeleted = await this.ventiradService.deleteVentirad(ventiradID);
        if (!ventiradDeleted) throw new NotFoundException('Ventirad does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Ventirad Deleted Successfully',
            ventiradDeleted
        });
    }

    // Update Ventirad: /update?ventiradID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createVentiradDTO: CreateVentiradDto, @Query('ventiradID') ventiradID) {
        const updatedVentirad = await this.ventiradService.updateVentirad(ventiradID, createVentiradDTO);
        if (!updatedVentirad) throw new NotFoundException('Ventirad does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Ventirad Updated Successfully',
            updatedVentirad
        });
    }
}
