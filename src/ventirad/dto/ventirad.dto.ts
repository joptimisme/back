export class CreateVentiradDto {
    readonly marque: String;
    readonly modele: String;
    readonly tdp: Number;
    readonly prix: Number;
  }
