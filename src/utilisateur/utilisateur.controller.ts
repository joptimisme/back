import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { UtilisateurService } from './utilisateur.service';
import { CreateUtilisateurDto } from './dto/utilisateur.dto';

@Controller('utilisateur')
export class UtilisateurController {
    constructor(private utilisateurService: UtilisateurService) {}

    // Add utilisateur: /utilisateur/create
    @Post('/create')
    async createUtilisateur(@Res() res, @Body() createUtilisateurDto: CreateUtilisateurDto) {
        const utilisateur = await this.utilisateurService.createUtilisateur(createUtilisateurDto);
        return res.status(HttpStatus.OK).json({
            message: 'Utilisateur Successfully Created',
            utilisateur
        });
    }

    // Get Utilisateurs /utilisateur
    // @Get('/list')
    @Get('/')
    async getUtilisateurs(@Res() res) {
        const utilisateurs = await this.utilisateurService.getUtilisateurs();
        return res.status(HttpStatus.OK).json(utilisateurs);
    }

    // GET single product: /utilisateur/5c9d46100e2e5c44c444b2d1
    @Get('/:utilisateurID')
    async getUtilisateur(@Res() res, @Param('utilisateurID') utilisateurID) {
        const utilisateur = await this.utilisateurService.getUtilisateur(utilisateurID);
        if (!utilisateur) throw new NotFoundException('Utilisateur does not exist!');
        return res.status(HttpStatus.OK).json(utilisateur);
    }

    // Delete Utilisateur: /delete?utilisateurID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteUtilisateur(@Res() res, @Query('utilisateurID') utilisateurID) {
        const utilisateurDeleted = await this.utilisateurService.deleteUtilisateur(utilisateurID);
        if (!utilisateurDeleted) throw new NotFoundException('Utilisateur does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Utilisateur Deleted Successfully',
            utilisateurDeleted
        });
    }

    // Update Utilisateur: /update?utilisateurID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createUtilisateurDTO: CreateUtilisateurDto, @Query('utilisateurID') utilisateurID) {
        const updatedUtilisateur = await this.utilisateurService.updateUtilisateur(utilisateurID, createUtilisateurDTO);
        if (!updatedUtilisateur) throw new NotFoundException('Utilisateur does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Utilisateur Updated Successfully',
            updatedUtilisateur
        });
    }
}
