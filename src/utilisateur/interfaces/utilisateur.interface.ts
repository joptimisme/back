import { Document } from 'mongoose';

export interface Utilisateur extends Document {
    readonly pseudo: String;
    readonly mail: String;
    readonly mdp: String;
}