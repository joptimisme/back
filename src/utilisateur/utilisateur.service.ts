import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Utilisateur } from './interfaces/utilisateur.interface';
import { CreateUtilisateurDto } from './dto/utilisateur.dto';

@Injectable()
export class UtilisateurService {
    constructor(@InjectModel('Utilisateur') private readonly utilisateurModel: Model<Utilisateur>) {}

    // Get all products
    async getUtilisateurs(): Promise<Utilisateur[]> {
        const utilisateurs = await this.utilisateurModel.find();
        return utilisateurs;
    }

    // Get a single Product
    async getUtilisateur(utilisateurID: string): Promise<Utilisateur> {
        const utilisateur = await this.utilisateurModel.findById(utilisateurID);
        return utilisateur;
    }

    // Post a single product
    async createUtilisateur(createUtilisateurDTO: CreateUtilisateurDto): Promise<Utilisateur> {
        const newUtilisateur = new this.utilisateurModel(createUtilisateurDTO);
        return newUtilisateur.save();
    }

    // Delete Utilisateur
    async deleteUtilisateur(utilisateurID: string): Promise<any> {
        const deleteUtilisateur = await this.utilisateurModel.findOneAndDelete(utilisateurID);
        return deleteUtilisateur;
    }

    // Put a single utilisateur
    async updateUtilisateur(utilisateurID: string, createUtilisateurDTO: CreateUtilisateurDto): Promise<Utilisateur> {
        const updatedUtilisateur = await this.utilisateurModel
            .findByIdAndUpdate(utilisateurID, createUtilisateurDTO, {new: true});
        return updatedUtilisateur;
    }
}
