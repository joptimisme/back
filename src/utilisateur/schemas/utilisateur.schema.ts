import { Schema } from 'mongoose';

export const UtilisateurSchema = new Schema({
    pseudo: ({type: String , required: true }),
    mail: ({type: String , required: true }),
    mdp: ({type: String , required: true , minlength: 8}),
}, {
    versionKey: false,
});
