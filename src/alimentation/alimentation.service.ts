import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Alimentation} from './interfaces/alimentation.interface';
import {CreateAlimentationDto} from './dto/alimentation.dto';

@Injectable()
export class AlimentationService {
    constructor(@InjectModel('Alimentation') private readonly alimentationModel: Model<Alimentation>) {}

    // Get all Alimentation
    async getAlimentations(): Promise<Alimentation[]> {
        const alimentations = await this.alimentationModel.find();
        return alimentations;
    }

    // Get a single Alimentation
    async getAlimentation(alimentationID: string): Promise<Alimentation> {
        const alimentation = await this.alimentationModel.findById(alimentationID);
        return alimentation;
    }

    // Post a single Alimentation
    async createAlimentation(createAlimentationDto: CreateAlimentationDto): Promise<Alimentation> {
        const newAlimentation = new this.alimentationModel(createAlimentationDto);
        return newAlimentation.save();
    }

    // Delete Alimentation
    async deleteAlimentation(alimentationID: string): Promise<any> {
        const deleteAlimentation = await this.alimentationModel.findOneAndDelete(alimentationID);
        return deleteAlimentation;
    }

    // Put a single Alimentation
    async updateAlimentation(alimentationID: string, createAlimentationDTO: CreateAlimentationDto): Promise<Alimentation> {
        const updatedAlimentation = await this.alimentationModel
            .findByIdAndUpdate(alimentationID, createAlimentationDTO, {new: true});
        return updatedAlimentation;
    }
}
