import { Schema } from 'mongoose';

export const AlimentationSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    puissance: ({type: Number , required: true }),
    format: ({type: String , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});
