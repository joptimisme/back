import { Module } from '@nestjs/common';
import { AlimentationController } from './alimentation.controller';
import { AlimentationService } from './alimentation.service';
import {AlimentationSchema} from './schemas/alimentation.schema';
import {MongooseModule} from '@nestjs/mongoose';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Alimentation', schema: AlimentationSchema }])],
    controllers: [AlimentationController],
    providers: [AlimentationService] ,
})
export class AlimentationModule {}
