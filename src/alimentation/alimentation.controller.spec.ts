import { Test, TestingModule } from '@nestjs/testing';
import { AlimentationController } from './alimentation.controller';

describe('Alimentation Controller', () => {
  let controller: AlimentationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AlimentationController],
    }).compile();

    controller = module.get<AlimentationController>(AlimentationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
