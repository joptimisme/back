export class CreateAlimentationDto {
    readonly marque: String;
    readonly modele: String;
    readonly puissance: Number;
    readonly format: String;
    readonly prix: Number;
}