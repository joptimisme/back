import { Document } from 'mongoose';

export interface Alimentation extends Document {
    readonly marque: String;
    readonly modele: String;
    readonly puissance: Number;
    readonly format: String;
    readonly prix: Number;
}