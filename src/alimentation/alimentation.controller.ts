import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import {CreateAlimentationDto} from './dto/alimentation.dto';
import {AlimentationService} from './alimentation.service';

@Controller('alimentation')
export class AlimentationController {
    constructor(private alimentationService: AlimentationService) {}

    // Add alimentation: /alimentation/create
    @Post('/create')
    async createAlimentation(@Res() res, @Body() createAlimentationDto: CreateAlimentationDto) {
        const alimentation = await this.alimentationService.createAlimentation(createAlimentationDto);
        return res.status(HttpStatus.OK).json({
            message: 'Alimentation Successfully Created',
            alimentation
        });
    }

    // Get Alimentations /alimentation
    // @Get('/list')
    @Get('/')
    async getAlimentations(@Res() res) {
        const alimentations = await this.alimentationService.getAlimentations();
        return res.status(HttpStatus.OK).json(alimentations);
    }

    // GET single product: /alimentation/5c9d46100e2e5c44c444b2d1
    @Get('/:alimentationID')
    async getAlimentation(@Res() res, @Param('alimentationID') alimentationID) {
        const alimentation = await this.alimentationService.getAlimentation(alimentationID);
        if (!alimentation) throw new NotFoundException('Alimentation does not exist!');
        return res.status(HttpStatus.OK).json(alimentation);
    }

    // Delete Alimentation: /delete?alimentationID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteAlimentation(@Res() res, @Query('alimentationID') alimentationID) {
        const alimentationDeleted = await this.alimentationService.deleteAlimentation(alimentationID);
        if (!alimentationDeleted) throw new NotFoundException('Alimentation does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Alimentation Deleted Successfully',
            alimentationDeleted
        });
    }

    // Update Alimentation: /update?alimentationID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createAlimentationDTO: CreateAlimentationDto, @Query('alimentationID') alimentationID) {
        const updatedAlimentation = await this.alimentationService.updateAlimentation(alimentationID, createAlimentationDTO);
        if (!updatedAlimentation) throw new NotFoundException('Alimentation does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Alimentation Updated Successfully',
            updatedAlimentation
        });
    }
}
