import { Schema } from 'mongoose';

export const StockageSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    capacite: ({type: Number , required: true }),
    format: ({type: String , required: true }),
    type: ({type: String , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});
/*
StockageSchema.static('findByMarque', function(marq) {
    return this.find({ marque: marq });
});*/
