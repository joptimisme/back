import { Document } from 'mongoose';

export interface Stockage extends Document {
    readonly marque: String;
    readonly modele: String;
    readonly capacite: Number;
    readonly format: String;
    readonly type: String;
    readonly prix: Number;
}