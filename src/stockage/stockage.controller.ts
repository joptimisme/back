import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { CreateStockageDto } from './dto/stockage.dto';
import { StockageService } from './stockage.service';

@Controller('stockage')
export class StockageController {
  constructor(private stockageService: StockageService) {}

  // Add stockage: /stockage/create
  @Post('/create')
  async createStockage(@Res() res, @Body() createStockageDto: CreateStockageDto) {
      const stockage = await this.stockageService.createStockage(createStockageDto);
      return res.status(HttpStatus.OK).json({
          message: 'Stockage Successfully Created',
          stockage
      });
  }

    // Get Stockages /stockage
    // @Get('/list')
    @Get('/')
    async getStockages(@Res() res) {
        const stockages = await this.stockageService.getStockages();
        return res.status(HttpStatus.OK).json(stockages);
    }

    // GET single product: /stockage/5c9d46100e2e5c44c444b2d1
    @Get('/:stockageID')
    async getStockageByID(@Res() res, @Param('stockageID') stockageID) {
        const stockage = await this.stockageService.getStockageByID(stockageID);
        if (!stockage) throw new NotFoundException('Stockage does not exist!');
        return res.status(HttpStatus.OK).json(stockage);
    }

    /*// GET single product: /stockage/find?mod=hp
    @Get('/find')
    async getStockage(@Res() res, @Query('marq') marq) {
        const stockage = await this.stockageService.getStockage(marq);
        if (!stockage) throw new NotFoundException('Stockage does not exist!');
        return res.status(HttpStatus.OK).json(stockage);
    }*/

    // Delete Stockage: /delete?stockageID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteStockage(@Res() res, @Query('stockageID') stockageID) {
        const stockageDeleted = await this.stockageService.deleteStockage(stockageID);
        if (!stockageDeleted) throw new NotFoundException('Stockage does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Stockage Deleted Successfully',
            stockageDeleted
        });
    }

    // Update Stockage: /update?stockageID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createStockageDTO: CreateStockageDto, @Query('stockageID') stockageID) {
        const updatedStockage = await this.stockageService.updateStockage(stockageID, createStockageDTO);
        if (!updatedStockage) throw new NotFoundException('Stockage does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Stockage Updated Successfully',
            updatedStockage
        });
    }
}
