export class CreateStockageDto {
    readonly marque: String;
    readonly modele: String;
    readonly capacite: Number;
    readonly format: String;
    readonly type: String;
    readonly prix: Number;
  }
