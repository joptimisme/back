import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StockageService } from './stockage.service';
import { StockageController } from './stockage.controller';
import { StockageSchema } from './schemas/stockage.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Stockage', schema: StockageSchema }])],
    providers: [StockageService],
    controllers: [StockageController] ,
})
export class StockageModule {}
