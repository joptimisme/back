import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Stockage } from './interfaces/stockage.interface';
import { CreateStockageDto } from './dto/stockage.dto';

@Injectable()
export class StockageService {
  constructor(@InjectModel('Stockage') private readonly stockageModel: Model<Stockage>) {}

    // Get all products
    async getStockages(): Promise<Stockage[]> {
        const stockages = await this.stockageModel.find();
        return stockages;
    }

    // Get a single Product
    async getStockageByID(stockageID: string): Promise<Stockage> {
        const stockage = await this.stockageModel.findById(stockageID);
        return stockage;
    }

/*
    // Get a single Product
    async getStockage(marq: string): Promise<Stockage> {
        const stockage = await this.stockageModel.findByMarque(marq);
        return stockage;
    }
*/

    // Post a single product
    async createStockage(createStockageDTO: CreateStockageDto): Promise<Stockage> {
      const newStockage = new this.stockageModel(createStockageDTO);
      return newStockage.save();
    }

    // Delete Stockage
    async deleteStockage(stockageID: string): Promise<any> {
        const deleteStockage = await this.stockageModel.findOneAndDelete(stockageID);
        return deleteStockage;
    }

    // Put a single stockage
    async updateStockage(stockageID: string, createStockageDTO: CreateStockageDto): Promise<Stockage> {
        const updatedStockage = await this.stockageModel
            .findByIdAndUpdate(stockageID, createStockageDTO, {new: true});
        return updatedStockage;
    }
}
