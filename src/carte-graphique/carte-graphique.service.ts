import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {CarteGraphique} from './interfaces/carte-graphique.interface';
import {CreateCarteGraphiqueDto} from './dto/carte-graphique.dto';

@Injectable()
export class CarteGraphiqueService {
    constructor(@InjectModel('CarteGraphique') private readonly cartegraphiqueModel: Model<CarteGraphique>) {}

    // Get all products
    async getCarteGraphiques(): Promise<CarteGraphique[]> {
        const cartegraphiques = await this.cartegraphiqueModel.find();
        return cartegraphiques;
    }

    // Get a single Product
    async getCarteGraphique(cartegraphiqueID: string): Promise<CarteGraphique> {
        const cartegraphique = await this.cartegraphiqueModel.findById(cartegraphiqueID);
        return cartegraphique;
    }

    // Post a single product
    async createCarteGraphique(createCarteGraphiqueDTO: CreateCarteGraphiqueDto): Promise<CarteGraphique> {
        const newCarteGraphique = new this.cartegraphiqueModel(createCarteGraphiqueDTO);
        return newCarteGraphique.save();
    }

    // Delete CarteGraphique
    async deleteCarteGraphique(cartegraphiqueID: string): Promise<any> {
        const deleteCarteGraphique = await this.cartegraphiqueModel.findOneAndDelete(cartegraphiqueID);
        return deleteCarteGraphique;
    }

    // Put a single cartegraphique
    async updateCarteGraphique(cartegraphiqueID: string, createCarteGraphiqueDTO: CreateCarteGraphiqueDto): Promise<CarteGraphique> {
        const updatedCarteGraphique = await this.cartegraphiqueModel
            .findByIdAndUpdate(cartegraphiqueID, createCarteGraphiqueDTO, {new: true});
        return updatedCarteGraphique;
    }
}
