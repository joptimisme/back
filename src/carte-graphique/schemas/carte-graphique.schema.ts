import { Schema } from 'mongoose';

export const CarteGraphiqueSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    longeur: ({type: Number , required: true }),
    largeur: ({type: Number , required: true }),
    hauteur: ({type: Number , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});
