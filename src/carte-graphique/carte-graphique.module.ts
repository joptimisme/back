import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CarteGraphiqueService } from './carte-graphique.service';
import { CarteGraphiqueController } from './carte-graphique.controller';
import {CarteGraphiqueSchema} from './schemas/carte-graphique.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'CarteGraphique', schema: CarteGraphiqueSchema }])],
    providers: [CarteGraphiqueService],
    controllers: [CarteGraphiqueController],
})
export class CarteGraphiqueModule {}
