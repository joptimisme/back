import { Test, TestingModule } from '@nestjs/testing';
import { CarteGraphiqueService } from './carte-graphique.service';

describe('CarteGraphiqueService', () => {
  let service: CarteGraphiqueService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CarteGraphiqueService],
    }).compile();

    service = module.get<CarteGraphiqueService>(CarteGraphiqueService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
