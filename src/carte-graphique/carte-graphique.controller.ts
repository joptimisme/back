import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import {CreateCarteGraphiqueDto} from './dto/carte-graphique.dto';
import {CarteGraphiqueService} from './carte-graphique.service';

@Controller('cartegraphique')
export class CarteGraphiqueController {
    constructor(private cartegraphiqueService: CarteGraphiqueService) {}

    // Add cartegraphique: /cartegraphique/create
    @Post('/create')
    async createCarteGraphique(@Res() res, @Body() createCarteGraphiqueDto: CreateCarteGraphiqueDto) {
        const cartegraphique = await this.cartegraphiqueService.createCarteGraphique(createCarteGraphiqueDto);
        return res.status(HttpStatus.OK).json({
            message: 'CarteGraphique Successfully Created',
            cartegraphique
        });
    }

    // Get CarteGraphiques /cartegraphique
    // @Get('/list')
    @Get('/')
    async getCarteGraphiques(@Res() res) {
        const cartegraphiques = await this.cartegraphiqueService.getCarteGraphiques();
        return res.status(HttpStatus.OK).json(cartegraphiques);
    }

    // GET single product: /cartegraphique/5c9d46100e2e5c44c444b2d1
    @Get('/:cartegraphiqueID')
    async getCarteGraphique(@Res() res, @Param('cartegraphiqueID') cartegraphiqueID) {
        const cartegraphique = await this.cartegraphiqueService.getCarteGraphique(cartegraphiqueID);
        if (!cartegraphique) throw new NotFoundException('CarteGraphique does not exist!');
        return res.status(HttpStatus.OK).json(cartegraphique);
    }

    // Delete CarteGraphique: /delete?cartegraphiqueID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteCarteGraphique(@Res() res, @Query('cartegraphiqueID') cartegraphiqueID) {
        const cartegraphiqueDeleted = await this.cartegraphiqueService.deleteCarteGraphique(cartegraphiqueID);
        if (!cartegraphiqueDeleted) throw new NotFoundException('CarteGraphique does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'CarteGraphique Deleted Successfully',
            cartegraphiqueDeleted
        });
    }

    // Update CarteGraphique: /update?cartegraphiqueID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createCarteGraphiqueDTO: CreateCarteGraphiqueDto, @Query('cartegraphiqueID') cartegraphiqueID) {
        const updatedCarteGraphique = await this.cartegraphiqueService.updateCarteGraphique(cartegraphiqueID, createCarteGraphiqueDTO);
        if (!updatedCarteGraphique) throw new NotFoundException('CarteGraphique does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'CarteGraphique Updated Successfully',
            updatedCarteGraphique
        });
    }
}
