import { Test, TestingModule } from '@nestjs/testing';
import { CarteGraphiqueController } from './carte-graphique.controller';

describe('CarteGraphique Controller', () => {
  let controller: CarteGraphiqueController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CarteGraphiqueController],
    }).compile();

    controller = module.get<CarteGraphiqueController>(CarteGraphiqueController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
