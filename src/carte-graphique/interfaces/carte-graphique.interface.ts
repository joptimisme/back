import { Document } from 'mongoose';

export interface CarteGraphique extends Document {
    readonly marque: String;
    readonly modele: String;
    readonly longeur: Number;
    readonly largeur: Number;
    readonly hauteur: Number;
    readonly prix: Number;
}