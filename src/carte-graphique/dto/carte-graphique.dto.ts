export class CreateCarteGraphiqueDto {
    readonly marque: String;
    readonly modele: String;
    readonly longeur: Number;
    readonly largeur: Number;
    readonly hauteur: Number;
    readonly prix: Number;
}