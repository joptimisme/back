import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StockageModule } from './stockage/stockage.module';
import { AlimentationModule } from './alimentation/alimentation.module';
import { BoitierModule } from './boitier/boitier.module';
import { CarteMereModule } from './carte-mere/carte-mere.module';
import { CarteGraphiqueModule } from './carte-graphique/carte-graphique.module';
import { RamModule } from './ram/ram.module';
import { ProcesseurModule } from './processeur/processeur.module';
import { VentiradModule } from './ventirad/ventirad.module';
import { UtilisateurModule } from './utilisateur/utilisateur.module';
import { CommandeModule } from './commande/commande.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/pcparts', {
      useNewUrlParser: true
    }),
    StockageModule,
    AlimentationModule,
    BoitierModule,
    CarteMereModule,
    CarteGraphiqueModule,
    RamModule,
    ProcesseurModule,
    VentiradModule,
    UtilisateurModule,
    CommandeModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
