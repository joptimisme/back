import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import {CreateCarteMereDto} from './dto/carte-mere.dto';
import {CarteMereService} from './carte-mere.service';

@Controller('cartemere')
export class CarteMereController {
    constructor(private cartemereService: CarteMereService) {}

    // Add cartemere: /cartemere/create
    @Post('/create')
    async createCarteMere(@Res() res, @Body() createCarteMereDto: CreateCarteMereDto) {
        const cartemere = await this.cartemereService.createCarteMere(createCarteMereDto);
        return res.status(HttpStatus.OK).json({
            message: 'CarteMere Successfully Created',
            cartemere
        });
    }

    // Get CarteMeres /cartemere
    // @Get('/list')
    @Get('/')
    async getCarteMeres(@Res() res) {
        const cartemeres = await this.cartemereService.getCarteMeres();
        return res.status(HttpStatus.OK).json(cartemeres);
    }

    // GET single product: /cartemere/5c9d46100e2e5c44c444b2d1
    @Get('/:cartemereID')
    async getCarteMere(@Res() res, @Param('cartemereID') cartemereID) {
        const cartemere = await this.cartemereService.getCarteMere(cartemereID);
        if (!cartemere) throw new NotFoundException('CarteMere does not exist!');
        return res.status(HttpStatus.OK).json(cartemere);
    }

    // Delete CarteMere: /delete?cartemereID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteCarteMere(@Res() res, @Query('cartemereID') cartemereID) {
        const cartemereDeleted = await this.cartemereService.deleteCarteMere(cartemereID);
        if (!cartemereDeleted) throw new NotFoundException('CarteMere does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'CarteMere Deleted Successfully',
            cartemereDeleted
        });
    }

    // Update CarteMere: /update?cartemereID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createCarteMereDTO: CreateCarteMereDto, @Query('cartemereID') cartemereID) {
        const updatedCarteMere = await this.cartemereService.updateCarteMere(cartemereID, createCarteMereDTO);
        if (!updatedCarteMere) throw new NotFoundException('CarteMere does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'CarteMere Updated Successfully',
            updatedCarteMere
        });
    }
}
