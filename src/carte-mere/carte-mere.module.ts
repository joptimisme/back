import { Module } from '@nestjs/common';
import { CarteMereService } from './carte-mere.service';
import { CarteMereController } from './carte-mere.controller';
import {MongooseModule} from '@nestjs/mongoose';
import {CarteMereSchema} from './schemas/carte-mere.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'CarteMere', schema: CarteMereSchema }])],
    providers: [CarteMereService],
    controllers: [CarteMereController]
})
export class CarteMereModule {}
