import { Test, TestingModule } from '@nestjs/testing';
import { CarteMereService } from './carte-mere.service';

describe('CarteMereService', () => {
  let service: CarteMereService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CarteMereService],
    }).compile();

    service = module.get<CarteMereService>(CarteMereService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
