export class CreateCarteMereDto {
    readonly marque: String;
    readonly modele: String;
    readonly format: String;
    readonly nb_sata: Number;
    readonly socket: String;
    readonly format_ram: String;
    readonly prix: Number;
}