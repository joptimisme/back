import { Test, TestingModule } from '@nestjs/testing';
import { CarteMereController } from './carte-mere.controller';

describe('CarteMere Controller', () => {
  let controller: CarteMereController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CarteMereController],
    }).compile();

    controller = module.get<CarteMereController>(CarteMereController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
