import { Schema } from 'mongoose';

export const CarteMereSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    format: ({type: String , required: true }),
    nb_sata: ({type: Number , required: true }),
    socket: ({type: String , required: true }),
    format_ram: ({type: String , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});
