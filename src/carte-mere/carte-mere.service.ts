import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCarteMereDto } from './dto/carte-mere.dto';
import { CarteMere } from './interfaces/carte-mere.interface';

@Injectable()
export class CarteMereService {
    constructor(@InjectModel('CarteMere') private readonly cartemereModel: Model<CarteMere>) {}

    // Get all products
    async getCarteMeres(): Promise<CarteMere[]> {
        const cartemeres = await this.cartemereModel.find();
        return cartemeres;
    }

    // Get a single Product
    async getCarteMere(cartemereID: string): Promise<CarteMere> {
        const cartemere = await this.cartemereModel.findById(cartemereID);
        return cartemere;
    }

    // Post a single product
    async createCarteMere(createCarteMereDTO: CreateCarteMereDto): Promise<CarteMere> {
        const newCarteMere = new this.cartemereModel(createCarteMereDTO);
        return newCarteMere.save();
    }

    // Delete CarteMere
    async deleteCarteMere(cartemereID: string): Promise<any> {
        const deleteCarteMere = await this.cartemereModel.findOneAndDelete(cartemereID);
        return deleteCarteMere;
    }

    // Put a single cartemere
    async updateCarteMere(cartemereID: string, createCarteMereDTO: CreateCarteMereDto): Promise<CarteMere> {
        const updatedCarteMere = await this.cartemereModel
            .findByIdAndUpdate(cartemereID, createCarteMereDTO, {new: true});
        return updatedCarteMere;
    }
}
