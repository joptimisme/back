import { Schema } from 'mongoose';

export const BoitierSchema = new Schema({
    marque: ({type: String , required: true }),
    modele: ({type: String , required: true }),
    format: ({type: String , required: true }),
    cg_max_size: ({type: Number , required: true }),
    /*hauteur: ({type: Number , required: true }),
    largeur: ({type: Number , required: true }),
    profondeur: ({type: Number , required: true }),*/
    dimension: ({type: [String], required: true}),
    format_stockage: ({type: String , required: true }),
    prix: ({type: Number , required: true }),
}, {
    versionKey: false,
});
