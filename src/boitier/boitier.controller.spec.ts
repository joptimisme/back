import { Test, TestingModule } from '@nestjs/testing';
import { BoitierController } from './boitier.controller';

describe('Boitier Controller', () => {
  let controller: BoitierController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BoitierController],
    }).compile();

    controller = module.get<BoitierController>(BoitierController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
