import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BoitierController } from './boitier.controller';
import { BoitierSchema } from './schemas/boitier.schema';
import { BoitierService} from './boitier.service';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Boitier', schema: BoitierSchema}])],
    providers: [BoitierService],
    controllers: [BoitierController],
})
export class BoitierModule {}
