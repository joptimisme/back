import { Document } from 'mongoose';

export interface Boitier extends Document {
    readonly marque: String;
    readonly modele: String;
    readonly format: String;
    readonly cg_max_size: Number;
    /*readonly hauteur: Number;
    readonly largeur: Number;
    readonly profondeur: Number;*/
    readonly dimension: [String];
    readonly format_stockage: String;
    readonly prix: Number;
}