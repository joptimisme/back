import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { BoitierService } from './boitier.service';
import { CreateBoitierDto } from './dto/boitier.dto';

@Controller('boitier')
export class BoitierController {
    constructor(private boitierService: BoitierService) {}

    // Add boitier: /boitier/create
    @Post('/create')
    async createBoitier(@Res() res, @Body() createBoitierDto: CreateBoitierDto) {
        const boitier = await this.boitierService.createBoitier(createBoitierDto);
        return res.status(HttpStatus.OK).json({
            message: 'Boitier Successfully Created',
            boitier
        });
    }

    // Get Boitiers /boitier
    // @Get('/list')
    @Get('/')
    async getBoitiers(@Res() res) {
        const boitiers = await this.boitierService.getBoitiers();
        return res.status(HttpStatus.OK).json(boitiers);
    }

    // GET single product: /boitier/5c9d46100e2e5c44c444b2d1
    @Get('/:boitierID')
    async getBoitier(@Res() res, @Param('boitierID') boitierID) {
        const boitier = await this.boitierService.getBoitier(boitierID);
        if (!boitier) throw new NotFoundException('Boitier does not exist!');
        return res.status(HttpStatus.OK).json(boitier);
    }

    // Delete Boitier: /delete?boitierID=5c9d45e705ea4843c8d0e8f7
    @Delete('/delete')
    async deleteBoitier(@Res() res, @Query('boitierID') boitierID) {
        const boitierDeleted = await this.boitierService.deleteBoitier(boitierID);
        if (!boitierDeleted) throw new NotFoundException('Boitier does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Boitier Deleted Successfully',
            boitierDeleted
        });
    }

    // Update Boitier: /update?boitierID=5c9d45e705ea4843c8d0e8f7
    @Put('/update')
    async updateProduct(@Res() res, @Body() createBoitierDTO: CreateBoitierDto, @Query('boitierID') boitierID) {
        const updatedBoitier = await this.boitierService.updateBoitier(boitierID, createBoitierDTO);
        if (!updatedBoitier) throw new NotFoundException('Boitier does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Boitier Updated Successfully',
            updatedBoitier
        });
    }
}
