import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import {CreateBoitierDto} from './dto/boitier.dto';
import {InjectModel} from '@nestjs/mongoose';
import {Boitier} from './interfaces/boitier.interface';

@Injectable()
export class BoitierService {
    constructor(@InjectModel('Boitier') private readonly boitierModel: Model<Boitier>) {}

    // Get all products
    async getBoitiers(): Promise<Boitier[]> {
        const boitiers = await this.boitierModel.find();
        return boitiers;
    }

    // Get a single Product
    async getBoitier(boitierID: string): Promise<Boitier> {
        const boitier = await this.boitierModel.findById(boitierID);
        return boitier;
    }

    // Post a single product
    async createBoitier(createBoitierDTO: CreateBoitierDto): Promise<Boitier> {
        const newBoitier = new this.boitierModel(createBoitierDTO);
        return newBoitier.save();
    }

    // Delete Boitier
    async deleteBoitier(boitierID: string): Promise<any> {
        const deleteBoitier = await this.boitierModel.findOneAndDelete(boitierID);
        return deleteBoitier;
    }

    // Put a single boitier
    async updateBoitier(boitierID: string, createBoitierDTO: CreateBoitierDto): Promise<Boitier> {
        const updatedBoitier = await this.boitierModel
            .findByIdAndUpdate(boitierID, createBoitierDTO, {new: true});
        return updatedBoitier;
    }
}
