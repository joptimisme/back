import { Test, TestingModule } from '@nestjs/testing';
import { BoitierService } from './boitier.service';

describe('BoitierService', () => {
  let service: BoitierService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BoitierService],
    }).compile();

    service = module.get<BoitierService>(BoitierService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
